# ForexBot

Welcome to the README for the Forex Trading Bot project, developed as a part of the bachelor's work. This project focuses on implementing an automated trading system for the foreign exchange (forex) market using a popular technical analysis strategy known as the Moving Average Crossover.

# Dependencies and Environment Setup
This project contains all necessary Python dependencies. To set up the environment named "myenv", follow these steps:

Navigate to the project directory in your terminal or command prompt.
Run the following command to create the virtual environment:

Activate the virtual environment:
On Windows:
* myenv\Scripts\activate

On Unix or MacOS:
bash
* source myenv/bin/activate

!! Please note that the usage of the OANDA API is restricted to the United States due to open-source regulations. To access and utilize the OANDA API for this project, it's essential to connect to a VPN server located within the USA.

